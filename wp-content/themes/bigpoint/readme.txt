Theme Name: Big Point
Theme URI: http://www.demo.lollum.com/bigpoint
Description: A Flexible Multi-Purpose Shop template. Big Point is responsive, highly customizable and suitable for any business, ecommerce or portfolio.
Tags: white, light, one-column, two-columns, three-columns, four-columns, left-sidebar, right-sidebar, custom-menu, featured-images, post-formats, sticky-post, translation-ready
Author: Lollum
Author URI: http://www.lollum.com/
License: GNU General Public License version 3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Version: 1.23

Changelog:

Big Point v1.23 - Sep 05, 2014

- Revolution Slider updated
- Minor fixes

Big Point v1.22 - Aug 22, 2014

- Minor fixes
- Minor fix for Firefox select
- Slider in shop page
- Lollum-Framework plugin updated

Big Point v1.21 - Aug 04, 2014

- SEO: minor fixes

Big Point v1.20 - Jul 24, 2014

- Lollum-Framework plugin updated

Big Point v1.19 - Jul 10, 2014

- Global option for the product sidebar
- Open social links in a new tab (optional)

Big Point v1.18 - Jul 04, 2014

- Minor Fix in account registration

Big Point v1.17 - Jun 28, 2014

- Framework plugin updated
- Minor Fixes

Big Point v1.16 - May 31, 2014

- VK sharer and icon in Theme Options
- Framework plugin updated
- Minor Fixes

Big Point v1.15 - May 23, 2014

- Minor fix in single jobs

Big Point v1.14 - May 20, 2014

- Font Awesome updated (v 4.1.0)
- Linecons font added
- Framework plugin updated

Big Point v1.13 - May 14, 2014

- Filter portfolio blocks by category
- Minor fix in countdown block
- Framework plugin updated
- Minor fixes

Big Point v1.12 - May 03, 2014

- WooCommerce 2.1.8 support
- Minor fixes

Big Point v1.11 - Apr 16, 2014

- WooCommerce 2.1.7 support

Big Point v1.10 - Apr 02, 2014

- Minor fix

Big Point v1.9 - Mar 27, 2014

- WooCommerce 2.1.6 support

Big Point v1.8 - Mar 25, 2014

- Minor fix for FitVids and Chrome
- Minor fixes

Big Point v1.7 - Mar 18, 2014

- Minor fix for FAQs filter
- Minor fix in product lightbox

Big Point v1.6 - Mar 02, 2014

- Minor fixes for WooCommerce 2.1.3

Big Point v1.5 - Mar 01, 2014

- WooCommerce 2.1.3 support

Big Point v1.4 - Feb 20, 2014

- Theme folder name corrected

Big Point v1.3 - Feb 19, 2014

- Save page elements in draft mode

Big Point v1.2 - Feb 17, 2014

- Minor fix in shipping methods

Big Point v1.1 - Feb 14, 2014

- Random sort in post/projects blocks
- Framework plugin updated
- Minor fixes for WooCommerce 2.1.2

Big Point v1.0 - Feb 12, 2014

- Initial release